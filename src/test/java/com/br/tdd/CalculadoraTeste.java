package com.br.tdd;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CalculadoraTeste {

        @Test
        public void testarSomaDeDoisNumero(){
                int resultado = Calculadora.soma(2, 2);
                Assertions.assertEquals(4, resultado);
        }
}
